{
    "id": "fd52b039-5679-4916-a48e-64773a45c299",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 10,
    "bbox_right": 51,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c6c58a1-f83d-42bc-b861-5fde139c309a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd52b039-5679-4916-a48e-64773a45c299",
            "compositeImage": {
                "id": "7f9c2270-176f-46b2-8c4e-e5ad4453e49c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c6c58a1-f83d-42bc-b861-5fde139c309a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e700f792-3608-414c-9515-f069a522fc7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6c58a1-f83d-42bc-b861-5fde139c309a",
                    "LayerId": "df70b7b4-79e1-4a57-acf0-47c4a93536d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "df70b7b4-79e1-4a57-acf0-47c4a93536d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd52b039-5679-4916-a48e-64773a45c299",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}