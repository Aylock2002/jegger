{
    "id": "141d8272-c18e-44db-b5c8-ea5a2b4045d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_indoors",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 231,
    "bbox_left": 0,
    "bbox_right": 423,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f5e506c-8a5e-43af-8d45-dc465b770d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141d8272-c18e-44db-b5c8-ea5a2b4045d0",
            "compositeImage": {
                "id": "155ebecf-dfa7-49bd-93d9-39d278baec05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5e506c-8a5e-43af-8d45-dc465b770d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1819c4-ed02-45b2-b1b4-55c8482e214e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5e506c-8a5e-43af-8d45-dc465b770d4c",
                    "LayerId": "1d53d17a-3c1d-4e28-b670-00361e7fa17b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "1d53d17a-3c1d-4e28-b670-00361e7fa17b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "141d8272-c18e-44db-b5c8-ea5a2b4045d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}