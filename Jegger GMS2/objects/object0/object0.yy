{
    "id": "45a42060-eba6-4c7c-8f23-570ce345313c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object0",
    "eventList": [
        {
            "id": "63997534-b026-4e7d-94ce-5c68c6e1adfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45a42060-eba6-4c7c-8f23-570ce345313c"
        },
        {
            "id": "835e9446-0bb9-486a-baaf-7b9792776705",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45a42060-eba6-4c7c-8f23-570ce345313c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "fd52b039-5679-4916-a48e-64773a45c299",
    "visible": true
}