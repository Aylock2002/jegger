{
    "id": "4194ffa3-8d70-4de9-9b6f-32d7c139856b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGun",
    "eventList": [
        {
            "id": "86249db7-2251-4af1-8512-9c00c3a79ae3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4194ffa3-8d70-4de9-9b6f-32d7c139856b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dbb2facf-fb3e-449a-a165-32b6a1b606bd",
    "visible": true
}