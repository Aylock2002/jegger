/// @desc

image_angle = point_direction(x, y, mouse_x, mouse_y);

if (mouse_check_button_pressed(mb_left)) {
	var bul = instance_create_layer(x+lengthdir_x(sprite_width,image_angle),y+lengthdir_y(sprite_width,image_angle),"Instances", oBullet);
	bul.speed = 6;
	bul.direction = image_angle;
}