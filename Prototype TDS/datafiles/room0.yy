
{
    "name": "room0",
    "id": "fa8b26e7-f6d3-4415-9c83-f2fc09eb1cff",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "a3027cd3-818b-425e-8f52-75aa715b4dba"
    ],
    "IsDnD": false,
    "layers": [
        {
            "name": "Folder_1",
            "id": "e87e29eb-bbc7-4ac8-abbd-7d233364ed75",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                {
                    "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Instances_1",
                    "id": "d23c115c-e756-4ccd-9684-6934175c09dc",
                    "depth": 100,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "instances": [
{"name": "inst_5D79C09F","id": "a3027cd3-818b-425e-8f52-75aa715b4dba","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5D79C09F","objId": "45a42060-eba6-4c7c-8f23-570ce345313c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 128,"y": 32}
                    ],
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRInstanceLayer",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                },
                {
                    "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Tiles_1",
                    "id": "59ef1821-5be8-4a6f-b29e-ed08a6d234dc",
                    "depth": 200,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRTileLayer",
                    "prev_tileheight": 10,
                    "prev_tilewidth": 16,
                    "mvc": "1.0",
                    "tiles": {
                        "SerialiseData": null,
                        "SerialiseHeight": 20,
                        "SerialiseWidth": 16,
                        "TileSerialiseData": [
                            2147483648,2,3,3,3,4,18,7,5,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            8,14,10,10,10,10,17,14,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            8,14,10,10,10,10,17,14,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            8,14,10,10,10,14,17,14,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            8,14,10,10,14,14,16,14,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            21,25,10,10,14,14,14,14,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,8,10,10,23,24,25,10,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,8,10,10,12,2147483648,8,10,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,22,24,24,30,2,4,10,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,1,2,4,15,10,10,10,19,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,8,10,10,10,10,10,10,10,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,8,10,23,24,24,24,24,24,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,8,10,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,8,10,12,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648
                        ]
                    },
                    "tilesetId": "fd9c5838-6013-4336-aab6-e4334683c7e7",
                    "userdefined_depth": false,
                    "visible": true,
                    "x": 0,
                    "y": 0
                },
                {
                    "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Background",
                    "id": "093aa00b-ae4f-4113-969f-320513fe81c6",
                    "animationFPS": 15,
                    "animationSpeedType": "0",
                    "colour": { "Value": 4278190080 },
                    "depth": 300,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "hspeed": 0,
                    "htiled": false,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRBackgroundLayer",
                    "mvc": "1.0",
                    "spriteId": "00000000-0000-0000-0000-000000000000",
                    "stretch": false,
                    "userdefined_animFPS": false,
                    "userdefined_depth": false,
                    "visible": true,
                    "vspeed": 0,
                    "vtiled": false,
                    "x": 0,
                    "y": 0
                }
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "3cbc0010-e2cb-4800-8df8-6d47bde8cb84",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "6c5ee573-c437-4bbd-ac86-0418834ae630",
        "Height": 768,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 1024
    },
    "mvc": "1.0",
    "views": [
{"id": "7e742cef-3aa7-4485-a356-5cc22abd759b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "344a1d70-af64-4e7a-bdf2-6e8ef4673888","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "72664a47-ddbe-4472-8a89-3afc3c0ae537","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e8fe80d4-76c1-4294-a45f-4fec8c0d637c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "dd77f079-2386-4ae1-89ae-f81427d9b172","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "49a34c03-a19e-468a-8aae-232d11d7b90d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "69ca3c22-228f-40a8-a70d-ae401f964fbd","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "54eb888b-e0ae-4c43-9a34-cf74b5898a9c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "8051ea4c-620e-43a6-b7a2-98cabbf985d1",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}