{
    "id": "b53784c0-24bf-4179-bb44-fffed42d895c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e57bf2e1-8ab8-41b2-baec-fb6129f0643d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b53784c0-24bf-4179-bb44-fffed42d895c",
            "compositeImage": {
                "id": "e387c475-5cae-43f4-9294-768da2dbf284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e57bf2e1-8ab8-41b2-baec-fb6129f0643d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18220730-2295-44de-af7f-21b37fbab9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e57bf2e1-8ab8-41b2-baec-fb6129f0643d",
                    "LayerId": "e5373fa5-64c4-4ccf-925f-a729678674a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "e5373fa5-64c4-4ccf-925f-a729678674a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b53784c0-24bf-4179-bb44-fffed42d895c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}