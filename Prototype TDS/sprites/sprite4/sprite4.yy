{
    "id": "a2aac8c1-829c-4e1b-b4f9-b4c400d7946e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 85,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9990f419-f7fb-45c5-b833-76bed67c0ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2aac8c1-829c-4e1b-b4f9-b4c400d7946e",
            "compositeImage": {
                "id": "dcf271ef-d689-49d4-a10e-a0790b9e54a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9990f419-f7fb-45c5-b833-76bed67c0ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14dd02bd-4b7d-4b9a-9b57-3c17bc921c1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9990f419-f7fb-45c5-b833-76bed67c0ed0",
                    "LayerId": "a0df732c-19ab-4489-8f72-0726838099aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "a0df732c-19ab-4489-8f72-0726838099aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2aac8c1-829c-4e1b-b4f9-b4c400d7946e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 0,
    "yorig": 0
}