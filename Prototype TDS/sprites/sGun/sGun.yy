{
    "id": "dbb2facf-fb3e-449a-a165-32b6a1b606bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b3238a8-ee95-4478-b48f-cef8e0bd89eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb2facf-fb3e-449a-a165-32b6a1b606bd",
            "compositeImage": {
                "id": "ea447196-8522-4bec-b123-cb9c50a9d2b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3238a8-ee95-4478-b48f-cef8e0bd89eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1ea2078-686b-40eb-9e4f-1d6071e8c9b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3238a8-ee95-4478-b48f-cef8e0bd89eb",
                    "LayerId": "38820b36-49a4-4433-a22b-040cb7803e31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "38820b36-49a4-4433-a22b-040cb7803e31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbb2facf-fb3e-449a-a165-32b6a1b606bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 1,
    "yorig": 0
}