{
    "id": "ff42b2b5-9735-4d37-9461-19a82878aede",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea75ef52-78ca-406e-9ef1-ef3d8ccb6503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff42b2b5-9735-4d37-9461-19a82878aede",
            "compositeImage": {
                "id": "0f9d56d2-07b3-46b7-9a30-ef393e89a20d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea75ef52-78ca-406e-9ef1-ef3d8ccb6503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a18445f-7fbc-43a9-9dac-cd4e118cb623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea75ef52-78ca-406e-9ef1-ef3d8ccb6503",
                    "LayerId": "476f3bde-9a27-45e4-bb7a-c5771e6fa799"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "476f3bde-9a27-45e4-bb7a-c5771e6fa799",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff42b2b5-9735-4d37-9461-19a82878aede",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}