{
    "id": "902f7679-b0ca-4c56-a142-52293c1de550",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileset_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 231,
    "bbox_left": 0,
    "bbox_right": 423,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0f54816-7a71-4c9d-9f0e-7e7d94ea0619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "902f7679-b0ca-4c56-a142-52293c1de550",
            "compositeImage": {
                "id": "39543a6a-d96e-4703-92d1-1fd18400e585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f54816-7a71-4c9d-9f0e-7e7d94ea0619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d361da-a72f-483b-b3e6-f80cdb2421fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f54816-7a71-4c9d-9f0e-7e7d94ea0619",
                    "LayerId": "d1123e07-22d4-4802-af06-96ce02e7bcd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "d1123e07-22d4-4802-af06-96ce02e7bcd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "902f7679-b0ca-4c56-a142-52293c1de550",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}